



/* Home About Anchor
============================================================*/
$(document).on('click', '.hdr_menu_ls > li.no-link a', function (e) {
	e.preventDefault();

	var offset = $('[data-home-about]').offset().top;

	$('html, body').animate({ scrollTop: offset }, 700);
});

/*========== Home About Anchor ==========*/









/* First Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.first_banner');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			pagination: false,
			transitionStyle : 'fade',
		});

		// Custom Navigation Events
		$('.first_banner_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.first_banner_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .first_banner');
	}
});
/*========== First Banner ==========*/







/* Second Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.second_banner');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			pagination: false,
			transitionStyle : 'fade',
			addClassActive: true,
			afterAction: function () {
				$('.second_banner_prod').removeClass('active').eq(this.owl.currentItem).addClass('active');
			}
		});

		// Custom Navigation Events
		$('.second_banner_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.second_banner_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .second_banner');
	}
});
/*========== Second Banner ==========*/





/* Home Partners
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.home_part_bnr');

		owl.owlCarousel({
			// autoPlay : 3000,
			stopOnHover: true,
			navigation: false,
			pagination: false,
			itemsCustom: [
				[0, 1],
				[480, 2],
				[768, 3],
				[1200, 5]
			],
			lazyLoad: true
		});

		// Custom Navigation Events
		$('.home_part_bnr_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.home_part_bnr_btn.next').click(function () {
			owl.trigger('owl.next');
		});
	}
	catch (e) {
		console.warn("Owl Carousel cannot find .home_part_bnr");
	}
});
/*========== Home Partners ==========*/








/* Catalog One Image
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.cat_one_it_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			pagination: false,
			// transitionStyle : 'fade',
		});



		// Custom Navigation Events
		$('.cat_one_it_bnr_btn.prev').click(function () {
			$(this).parent().siblings('.cat_one_it_bnr').trigger('owl.prev');
		});

		$('.cat_one_it_bnr_btn.next').click(function () {
			$(this).parent().siblings('.cat_one_it_bnr').trigger('owl.next');
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .cat_one_it_bnr');
	}
});
/*========== Catalog One Image ==========*/

















/* Header
============================================================*/

/*========== Header ==========*/


